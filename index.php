<?php include 'header.php'?>
<article class='left-panel'>
<h2>The Stack</h2>
A little about the tools I use and why. This is a
more detailed description based on my 
<a href='https://docs.google.com/document/d/1k5bL6RYLB1PU6WTuHFsYcvTcKTtE05997NeBoWOHdQo/pub'>resume</a>.
<h5>LESS</h5>
I use the LESS processor for my CSS.
LESS extends CSS. It enables a programmer to use
variables and inheritance in their stylesheets. 
Other CSS frameworks try to replace CSS.
With LESS, the end result still resembles CSS. I believe this 
makes it easier for others to read. LESS is a snap to learn.
<h5>PHP</h5>
PHP was used for the backend.
While I prefer using Python it is hard
to argue with the success of PHP.
In the end it is not about the tools you use
but the what you create from them. The college
server running this site only supported CGI or PHP
for backend. It did give me a good excuse to pick
up some PHP. I would still pick Django over it.
<h5>JQuery/Javascript</h5>
I use JQuery a lot at work. It makes handling the DOM
a snap. The only negative about it is that it is an abstraction.
I have been trying to learn vanilla DOM on the side. 
I actually like javascript as a language; I love how it handles closures.
I have messed with Node.js for fun.
<h5>C/C++</h5>
I program in more C than C++. I use C++ at work
but because it is on an embedded project and compile times
are an issue we primiarly use C++ as C with classes.
I have been learning boost but I believe that when
the program needs the power of boost I would prefer to
switch to Python and use C/C++ when speed is needed.
Templates in C++ are powerful but overuse makes the code
difficult to follow.
<h5>Python</h5>
Python is my favorite general purpose glue language.
It is executable psuedo code. The problem with Python
is that it is a very slow. Which is why I think Python works
perfect as an embedded language.
<h5>Linux</h5>
I enjoy using Linux. My main computer I use is a
$250 google chromebook that uses <a href='https://github.com/dnschneid/crouton'>crouton</a> to have a 
fully working Linux distro chroot. I actually like windows 8 and think Apple makes the best
looking laptops around. Still, I enjoy the open source of Linux and the community more.
<h5>Other</h5>
I have fooled around with Ruby, Lisp and Java. I choose tools based on 
who am I working with. I think that specializing in a language has benefits but
90%(arbitrary high precent to make a point) of a language is fundamentally the same as any other. I always
write in psuedo code when designing my algorithms. There are some
exceptions but only in the extreme cases of 'pure functional' languages.
Which is why I messed with Lisp a bit. I plan to one day explore Haskell.
</article>
<?php include 'footer.php';?>

