<!doctype html>
<head>
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<title>Jack Muratore</title>
<link rel='stylesheet' href='//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css'>
<script src='//code.jquery.com/jquery-1.9.1.js'></script>
<script src='//code.jquery.com/ui/1.10.3/jquery-ui.js'></script>
<link rel='stylesheet/less' type='text/css' href='assets/style.less'>
<script src='assets/less-1.5.0.min.js'></script>
<script src='assets/jaws.js'></script>
</head>
<body>
<main>
<header>
<h1>Jack Muratore</h1>
<h4>
Full Stack Developer
</h4>
<nav>
<a href='index.php'><button>Stack</button></a>
<a href='space_invader.php'><button>Box Invader</button></a>
<a href='conway_life.php'><button>Conway's Life</button></a>
<a href='https://bitbucket.org/Banjocat'><button>Code</button></a>
<a href='https://docs.google.com/document/d/1k5bL6RYLB1PU6WTuHFsYcvTcKTtE05997NeBoWOHdQo/pub'><button>Resume</button></a>
</nav>
<script>
$('button').button();
</script>
</header>
